import asyncio
from . import helper_functions as hf
# from .static import Staticstuff as s


def async_run_at_noon(func):
    async def wrapper(self, *args, **kwargs):
        while True:
            await asyncio.sleep(hf.seconds_until_noon())
            await func(self, *args, **kwargs)
            await asyncio.sleep(5)  # seconds_until_noon() will return 0 if it's exactly 12:00:00
    return wrapper


class async_run_every_seconds(object):
    def __init__(self, seconds):
        print("init")
        self.s = seconds

    def __call__(self, func):
        print("call")
        async def wrapper(s, *args, **kwargs):  # h classes suck for this
            print("wrap")
            while True:
                await func(s, *args, **kwargs)
                await asyncio.sleep(self.s)
        return wrapper


def wait_until_ready(func):
    async def wrapper(self, *args, **kwargs):
        await self.wait_until_ready()
        await func(self, *args, **kwargs)
    return wrapper


def _async_run_every_second(func):
    """owo testing"""
    async def wrapper(self, *args, **kwargs):
        while True:
            await func(self, *args, **kwargs)
            await asyncio.sleep(1)
    return wrapper
